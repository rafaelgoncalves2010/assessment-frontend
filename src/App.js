import React from 'react';
import RegisterBar from './components/registerBar';
import SearchBar from './components/searchBar';
import Menu from './components/menu';
import RouteInformation from './components/routerInformation';
import Footer from './components/footer';
import MainContainer from './components/mainContainer';
import GlobalStyle from './styles/global'

function App() {
  return (
    <>
      <RegisterBar/>
      <SearchBar/>
      <Menu/>
      <RouteInformation/>
      <MainContainer/>
      <Footer/>
      <GlobalStyle/>
    </>  
  );
}

export default App;
