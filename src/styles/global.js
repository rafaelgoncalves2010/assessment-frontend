import { createGlobalStyle } from 'styled-components';
import styled from 'styled-components';
import { device } from './devices';


export default createGlobalStyle`
    *{
        margin: 0;
        padding: 0;
        outline: 0;
        box-sizing: border-box;
        font-family: 'Open Sans', sans-serif;
        text-rendering: optimizeLegibility;
    }
`;

export const Wrapper = styled.div`

    width: 100%;
    margin: 0 auto;

    @media ${device.desktop} {
        max-width: 1240px;

    }

    @media ${device.laptop} {
        max-width: 990px;
    }

    @media ${device.tablet} {
        max-width: 730px;
    }

    @media ${device.mobile} {
        max-width: 290px;

    }
`;