
import { useState } from 'react';

export default function SharedCategory(){
    const [ id, setId ] = useState(1);
    const [ idSideBar, setIdSideBar ] = useState(1);
    const [listMenu , setListMenu ] = useState([]);
    const [ products , setProducts ] = useState([]);
    const [ filterArr, setFilterArr ] = useState([]);
    const [ category, setCategory ] = useState('Camisetas');
    const [ newProducts, setNewProducts ] = useState([]);

    return{
        id,
        setId,
        listMenu,
        setListMenu,
        category,
        setCategory,
        products,
        setProducts,
        newProducts,
        setNewProducts,
        filterArr,
        setFilterArr,
        idSideBar,
        setIdSideBar
    };
};