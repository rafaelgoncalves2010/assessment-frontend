import styled from 'styled-components';
import { device } from '../../styles/devices';


export const OrderBy = styled.div`
    width: 100%;
    flex-direction: initial;

    h2{
        color: #b31327;
        font-size: 30px;
        font-weight: 400;
        line-height: 30px;
    }

    div{
        flex-direction: initial;
        margin-bottom: 35px;
        margin-top: 10px;
        display: flex;
        border-top: 1px solid #e0dedf;
        border-bottom: 1px solid #e0dedf;
        justify-content: space-between;
        align-items: center;

        @media ${device.mobile} {
            div{
                img{
                    position: absolute;
                    left: -2000000px;
                }
            }
        }

        div{
            border: none;
            margin: 0;
            width: auto;
            display: flex;
            align-items: center;

            span{
                text-transform: uppercase;
                font-size: 10px;
                color: #78797b;
                margin-right: 15px;
            }

            div{
                div{
                    div{
                        &:first-child{
                            font-size: 12px;
                            width: 115px;
                        }
                        }
                    }
                }
            }
  
        }
    }
`;

