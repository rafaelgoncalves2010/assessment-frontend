import React from 'react';
import { OrderBy } from './style';
import { useBetween } from 'use-between';
import SharedCategory from '../sharedCategory';
import Select from 'react-select';
import structureIcon from '../../assets/estruturaIcon.png';


export default function OrderFilterBy(){

    const { category } = useBetween(SharedCategory);
    const { products , setProducts } = useBetween(SharedCategory);
    const options = [
        { value: '1', label: 'Maior preço' },
        { value: '2', label: 'Menor preço' },
      ];

    function ordenarItens(selected){
        let newProducts = products;

        if(selected === "1"){
            newProducts.sort(function (a, b) {
                return b.price < a.price ? -1 : b.price > a.price ? 1 : 0;
            });
        }if(selected === "2"){
            newProducts.sort(function (a, b) {
                return a.price < b.price ? -1 : a.price > b.price ? 1 : 0;
            }); 
        }
         setProducts([...newProducts]);
    }

    return ( 

        <OrderBy>
            <h2>{ category }</h2>
            <div>
                <div>
                    <img alt="Imagem de divisão de layout" src={structureIcon}/>
                </div>

                <div>
                    <span>Ordernar por</span>
                    <Select placeholder={<p>Preço</p>} options={options} onChange={(e) => ordenarItens(e.value)}/>    
                </div>
            </div>
        </OrderBy>
    );
}