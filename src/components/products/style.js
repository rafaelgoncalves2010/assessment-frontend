import styled from 'styled-components';
import { device } from '../../styles/devices';

export const ProductStyle = styled.div`
display:flex;
flex-wrap: wrap;
width: auto;
justify-content: space-between;


    div{
        margin-bottom: 10px;
        display: flex;
        align-items: center;
        flex-direction: column;

        @media ${device.mobile} {
            width: 140px; 
            text-align: center; 
        }
        
        p{
            color: #606060;
            text-transform: uppercase;

            @media ${device.mobile} {
                height: 50px; 
            }
        }

        span{
            margin: 20px 0 10px 0;
            color: #b1141e;
            font-weight: 800;
        }

        button{
            background-color: #93bcb7;
            color: #ffffff;
            font-weight: 800;
            width: 100%;
            border: unset;
            padding: 10px 0;
            text-transform: uppercase;
            border-radius: 3px;
            margin-bottom: 30px;
            cursor: pointer;
        }

        div{
            height: 240px;
            display: flex;
            justify-content: center;
            align-items: center;
            border: 1px solid #e0dedf;
            margin-right: 0;
            
            @media ${device.mobile} {
                height: 140px;  
            }

            img{
                width: 60%;

                @media ${device.mobile} {
                    margin-right: 0px; 
                }
            }
        }
    }          
`;
