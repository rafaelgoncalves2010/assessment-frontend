import React, { useEffect } from 'react';
import { ProductStyle } from './style';
import api from '../../services/api';
import { useBetween } from 'use-between';
import SharedCategory from '../sharedCategory';

export default function Products(){
    
    const { products , setProducts, setNewProducts,setListMenu, id, setFilterArr, idSideBar } = useBetween(SharedCategory);
     

    useEffect(() =>{

        api.get(`/api/V1/categories/${idSideBar}`).then( response => {
            setFilterArr(response.data.items);
        });

        api.get(`/api/V1/categories/${id}`).then( response => {
            setProducts(response.data.items);
            setNewProducts(response.data.items);
        });

        api.get('/api/V1/categories/list').then( response => {
            setListMenu(response.data.items);
        });

        


    },[id, idSideBar, setFilterArr, setListMenu, setNewProducts, setProducts])

    return ( 
        <ProductStyle>
            {products.map( product => (  
                <div key={product.id}>
                    <div>
                        <img alt="Logo" src={product.image}/>
                    </div>
                    <p>{product.name}</p>
                    <span>R$ {product.price}</span>
                    <button>Comprar</button> 
                </div>                                    
            ))}
        </ProductStyle> 
    );
}