import React from 'react';
import { MainContainerStyle, Container } from './style';
import { Wrapper } from '../../styles/global';
import SideBar from '../sideBar';
import OrderFilterBy from '../orderFilterBy';
import Products from '../products';


export default function MainContainer(){
    return ( 
        <Wrapper>
            <MainContainerStyle>
                <SideBar/>
                <Container>
                    <OrderFilterBy />
                    <Products/>
                </Container>
            </MainContainerStyle>
        </Wrapper>
    );
}