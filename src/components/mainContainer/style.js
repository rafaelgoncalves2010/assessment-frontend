import styled from 'styled-components';
import { device } from '../../styles/devices';

export const MainContainerStyle = styled.div`
    display: flex;

    @media ${device.mobile} {
        flex-direction: column;
    }
`;

export const Container = styled.div`
    width: 100%;
`;


