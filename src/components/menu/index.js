import React from 'react';
import { MenuStyle } from './style';
import { Wrapper } from '../../../src/styles/global'
import { useBetween } from 'use-between';
import SharedCategory from '../sharedCategory';

export default function Menu(){
    const { setId, listMenu, setCategory, newProducts, setProducts } = useBetween(SharedCategory);

    function changeId(newId, newCategory){
       setProducts(newProducts);
       setId(newId);
       setCategory(newCategory);
    }

    return ( 
        <MenuStyle>
           <Wrapper>
                <ul>
                    <li>Página Inicial</li>
                        {listMenu.map( el =>(
                            <li key={el.id} onClick={() => changeId(el.id, el.name)}>{el.name}</li>
                        ))}
                    <li>Contato</li>
                </ul>
           </Wrapper>
        </MenuStyle>
    );
}