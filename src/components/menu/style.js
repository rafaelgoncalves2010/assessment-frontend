import styled from 'styled-components';
import { device } from '../../styles/devices';


export const MenuStyle = styled.div`
    background: red;
    
    @media ${device.mobile} {
        display: none;
    }
    
    ul{
        height: 55px;
        display: flex;
        justify-content: normal;
        align-items: center;
        text-transform: uppercase;
        color: #ffffff;
        list-style: none;
        font-weight: 600;

        @media ${device.tablet} {
            justify-content: space-between;

        }
    }

    li{
        margin-right: 80px; 
        cursor: pointer;

        @media ${device.tablet} {
            margin-right: 0px; 
        }
    }
`;