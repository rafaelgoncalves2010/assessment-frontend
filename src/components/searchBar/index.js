import React, { useState } from 'react';
import { SearchBarStyle, Form, ModalStyle, ModalNotFoundStyle } from './style';
import { Wrapper } from '../../../src/styles/global';
import logo from '../../assets/logo.jpg';
import lupa from '../../assets/lupa.svg';
import { useBetween } from 'use-between';
import SharedCategory from '../sharedCategory';
import { useForm } from "react-hook-form";
import HamburgerMenu from 'react-hamburger-menu';
import Modal from 'react-awesome-modal';

export default function SearchBar(){
    const { newProducts, setProducts, listMenu, setCategory, setId } = useBetween(SharedCategory);
    const { handleSubmit, register } = useForm();
    const [ statusModalHamburguer, setStatusModalHamburguer ] = useState(false);
    const [ statusModalLupa, setStatusModalLupa ] = useState(false);
    const [ statusModalNotFound, setStatusModalNotFound ] = useState(false);

    const onSubmit = (values, element) => {   

        const res =  values.searchedWordMobile ? values.searchedWordMobile : values.searchedWord;
        element.target.firstChild.value = "";

        const newArray = [];     

        if(res.length){

            newProducts.map(el => {
                if(el.name.toLowerCase().includes(res.toLowerCase())) newArray.push(el);
            });

            if (!newArray.length){
                setProducts(newProducts);
                changeStatusModalNotFound();

            }else{
                setProducts(newArray);
            }

        }else{
            setProducts(newProducts);
            changeStatusModalNotFound();
        }
    }

    const changeStatusModalHamburguer = () =>{
        setStatusModalHamburguer(!statusModalHamburguer);
    }

    const changeStatusModalLupa = () =>{
        setStatusModalLupa(!statusModalLupa);
    }

    const changeStatusModalNotFound = () =>{
        setStatusModalNotFound(!statusModalNotFound);
    }

    const changeId = (newId, newCategory) => {
        setProducts(newProducts);
        setId(newId);
        setCategory(newCategory);
        changeStatusModalHamburguer();
     }

    return ( 
        <>
        <SearchBarStyle>
            <Modal visible={statusModalHamburguer} effect="fadeInUp" width="90%" height="70%"  onClickAway={changeStatusModalHamburguer}> 
                 <ModalStyle>
                    <ul>
                        <li onClick={changeStatusModalHamburguer}>Página Inicial</li>
                            {listMenu.map( el =>(
                                <li key={el.id} onClick={() => changeId(el.id, el.name)}>{el.name}</li>
                            ))}
                        <li onClick={changeStatusModalHamburguer}>Contato</li>
                    </ul>
                </ModalStyle>
            </Modal>

            <Modal visible={statusModalLupa} effect="fadeInUp" width="90%" height="15%"  onClickAway={changeStatusModalLupa}>      
                 <Form onSubmit={handleSubmit(onSubmit)}>
                    <input ref={register} placeholder="Digite aqui" name="searchedWordMobile" />
                    <button type="submit" onClick={changeStatusModalLupa}>Buscar</button>
                </Form>
            </Modal>

            <Wrapper>
                <div>
                    <div>
                        <HamburgerMenu
                            isOpen={statusModalHamburguer}
                            menuClicked={changeStatusModalHamburguer}
                            width={20}
                            height={15}
                            strokeWidth={3}
                            rotate={0}
                            color='#000000'
                            borderRadius={0}
                            animationDuration={0.5}
                            
                        />
                    </div>

                    <div>
                        <img alt="Logo" src={logo} />
                    </div>

                    <div onClick={changeStatusModalLupa}>  
                        <img alt="Lupa" src={lupa} />  
                    </div>
                    
                    <div>
                        <Form onSubmit={handleSubmit(onSubmit)}>
                            <input ref={register} name="searchedWord" />
                            <button type="submit">Buscar</button>
                        </Form>
                    </div>
                </div>
           </Wrapper>

        </SearchBarStyle>
          <Modal visible={statusModalNotFound} effect="fadeInUp" width="300px" height="200px"  onClickAway={changeStatusModalNotFound}> 
            <ModalNotFoundStyle>   
                <label>Nenhum produto encontrado com esse nome.</label>            
                <button onClick={changeStatusModalNotFound}>OK</button>
            </ModalNotFoundStyle>  
        </Modal>
</>
    );
}