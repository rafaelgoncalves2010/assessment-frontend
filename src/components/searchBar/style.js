import styled from 'styled-components';
import { device } from '../../styles/devices';

export const SearchBarStyle = styled.div`
    background: #ffffff;
    padding-top: 25px;
    height: 115px;

    div{
        width: 100%;
        display: flex;
        
        div{
            width: 100%;
            display: flex;

            div{
                &:nth-child(1){
                    display: none;

                    @media ${device.mobile} {
                        display: flex;
                            div{
       
                                top: 40%;
                                left: 10%;
                        }
                    }
                } 

                &:nth-child(2){
                    width: auto;
                    display: block;
                } 

                &:nth-child(3){
                    display: none;

                    @media ${device.mobile} {
                        display: flex;
                        justify-content: flex-end;

                        img{
                            width: 25px;  
                        }
                    }
                }  

                &:nth-child(4){
                    width:100%;
                    display: flex;
                    justify-content: flex-end;
                    align-items: center;

                    @media ${device.mobile} {
                        display: none;
                    }
                }  
            }
        }
    }
`;

export const Form = styled.form`

    @media ${device.mobile} {
        display: flex;
        width: 100%;
        flex-direction: column;
    }   

    input{
        width: 415px;
        height: 45px;
        border-radius: unset;
        border:1px solid #939393;
        padding: 0 20px;

        @media ${device.mobile} {
            width: 100%;
        }

        @media ${device.tablet} {
            width: 285px;
        }
    }

    button{
        height: 45px;
        background: #ad1524;
        text-transform: uppercase;
        color: #ffffff;
        font-weight: 800;
        border: none;
        padding: 0 25px;
        cursor: pointer;

        @media ${device.mobile} {
            &:nth-child(3){
                border-top: 1px solid #fff;
            }
        } 
    }

`;

export const ModalStyle = styled.div`
    width: "90%"; 
    height: "70%";  

    ul{
        display: flex;
        flex-direction: column;
        list-style: none;
        justify-content: space-around;
        align-items: center;
        width: 100%;
        background: brown;
        color: white;
    }      
`;

export const ModalNotFoundStyle = styled.div`
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
padding: 20px;
    text-align: center;

    label{
        font-size: 20px;
    }

    button{
        width: 200px;
        height: 45px;
        background: #ad1524;
        text-transform: uppercase;
        color: #ffffff;
        font-weight: 800;
        border: none;
        padding: 0 25px;
        cursor: pointer;
        margin-top: 20px;
    }

`;

