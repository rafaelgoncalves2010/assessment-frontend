import React from 'react';
import { RegisterBarStyle } from './style';
import { Wrapper } from '../../styles/global';

export default function RegisterBar(){
    return ( 
        <RegisterBarStyle>
           <Wrapper>
               <div>
                    <span> <a href={"/"}>Acesse sua Conta</a> ou <a href={"/"} >Cadastre-se</a> </span>
               </div>
           </Wrapper>
        </RegisterBarStyle>
    );
}