import styled from 'styled-components';
import { device } from '../../styles/devices';

export const RegisterBarStyle = styled.header`
    width: 100%;
    background: #272526;

    div{
        display: flex;
        justify-content: flex-end;

        @media ${device.mobile} {
            justify-content: center;
        }

        span {
            font-size: 16px;
            margin: 5px 0;
            color: #fff;
        }

        a{
            font-size: 16px;
            margin: 5px 0;
            color: #fff;
        }
    }
`;



