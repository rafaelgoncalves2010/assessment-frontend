import styled from 'styled-components';
import { device } from '../../styles/devices';

export const RouteInformation = styled.div`
    height: 70px;
    display: flex;
    align-items: center;

    @media ${device.mobile} {
        height: 50px;
        font-size: 14px;
    }

    span:nth-child(2){
        color: #ae1524;
    }
`;

