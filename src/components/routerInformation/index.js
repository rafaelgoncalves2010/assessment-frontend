import React from 'react';
import {  RouteInformation } from './style';
import { Wrapper } from '../../styles/global';
import { useBetween } from 'use-between';
import SharedCategory from '../sharedCategory';

export default function RouterInformation(){
    
    const { category } = useBetween(SharedCategory);

    return ( 
        <Wrapper>
            <RouteInformation>
                <span>Página inicial &nbsp; {" > "} &nbsp; </span>
                <span> { category } </span>
            </RouteInformation>
        </Wrapper>
    );
}