import React from 'react';
import { SideBarStyle } from './style';
import { useBetween } from 'use-between';
import SharedCategory from '../sharedCategory';

export default function SideBar(){

    const { setProducts,filterArr,setIdSideBar } = useBetween(SharedCategory);

    function showResults(element, value){
       
        setIdSideBar(element.id, value);
        const newValues = [];

        if(filterArr.length && value.length){
            filterArr.map(el => {
                el.filter.map( e => {
                    if(e[element.property]){
                        if(e[element.property].toLowerCase() === value.toLowerCase()){
                            newValues.push(el);
                        } 
                    }
                })
            });
    
            setProducts(newValues)
        }

    }

    const sideBar = [
        {
            id: 2,
            category: "Gênero",
            values: [ "Masculina", "Feminina"],
            property: "gender"
        },
        {
            id: 1,
            category: "Cor",
            values: [ "preta", "laranja", "amarela", "rosa"],
            property: "color"
        },
        {
            id: 3,
            category: "Tipo",
            values: [ "Casual", "Corrida"],
            property: "type"
        }
    ]

    return ( 

    <SideBarStyle>
        {sideBar.map(el => (
            <>     
                <h4 key={el.category}>{el.category}</h4>
                <ul key={el.id}>
                    {el.values.map(outEl => (
                         <li key={outEl} onClick={() => showResults(el, outEl)}>{outEl}</li>
                    ))}
                </ul>
            </>
        ))}
    </SideBarStyle>);
}