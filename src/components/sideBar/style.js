import styled from 'styled-components';
import { device } from '../../styles/devices';

export const SideBarStyle = styled.aside`
    min-width: 260px;
    border: 1px solid #e0dedf;
    color: #626262;
    padding: 20px;
    height: 340px;
    margin-right: 30px;
    flex-direction: column;

    @media ${device.laptop} {
        min-width: 220px;
        margin-right: 20px;
    }

    @media ${device.tablet} {
        min-width: 205px;
    }

    @media ${device.mobile} {
        width: 100%;
        margin-bottom: 30px;
    }

    h3,h4{
        text-transform: uppercase;
    }

    h3{
        color: #ae1524;
        margin-bottom: 20px;
    }

    h4{
        color: #6f8d89;
        margin-bottom: 10px;
        font-weight: 600;
    }

    ul{
        padding-left: 15px;
        margin-bottom: 20px;
        margin-right: 40px;



        li {
            list-style-type: none;
            position: relative;  
            font-weight: 100;
            margin-bottom: 5px;
            cursor: pointer;

            &:before  {
                content: "·";
                font-size: 30px;
                line-height: 0.7;
                left: -10px;
                position: absolute;
            }  
        }

        &:nth-child(4){ 
        display: flex;
        padding-left: 0;
        

            li{
                width: 40px;
                height: 20px;
                margin-right: 3px;
                background-color: green;
                cursor: pointer;
                font-size: 0;

                &:before  {
                    display: none;
                }  

                &:nth-child(1){
                    background-color: #000000;
                } 

                &:nth-child(2){
                    background-color: #c65c4d;

                } 
                &:nth-child(3){
                    background-color: #df8c27;

                } 
                &:nth-child(4){
                    background-color: #c7bde5;

                } 
                
            }
       
        }




    }
`;


